# quizzler_game

This project demonstrates the use of Stateless and Stateful Widgets in Flutter.
User is expected to answer a series of quiz questions and his score is maintained.

The project uses OOPS concept such as Abstraction, Encapsulation, Inheritance.

It also uses various types of functions and the use of external packages.

Kindly refer for more details.

![quizzler](images/quizzler_game.png)
